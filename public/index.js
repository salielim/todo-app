(function() {
    var TodoApp = angular.module("TodoApp", []);

    var TodoCtrl = function() {
        var todoCtrl = this;

        todoCtrl.todoItem = "";
        todoCtrl.searchText = "";
        todoCtrl.todoList = [];

        todoCtrl.addToList = function() {
            todoCtrl.todoList.push(todoCtrl.todoItem);
            todoCtrl.todoItem = "";
            console.log(todoCtrl.todoList);
        }

        todoCtrl.delete = function($index) {
            todoCtrl.todoList.splice($index, 1);
        }
    };

    TodoApp.controller("TodoCtrl", TodoCtrl);
})();